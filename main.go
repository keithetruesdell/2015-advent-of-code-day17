package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
)

const (
	ex1   string = "input/ex1.txt"
	input string = "input/input.txt"
)

func main() {
	var (
		answer int
	)

	e := getFlags()
	f2r := file2Read(e)
	fData, err := readFile(f2r)
	if err != nil {
		fmt.Printf("ERROR:\n    %v\n", err)
	}

	fmt.Println("=======================")
	fmt.Printf("Answer: %v\n", answer)
}

// readFile -
func readFile(f2r string) (fData []int, err error) {

	f, err := os.Open(f2r)
	if err != nil {
		return fData, err
	}
	defer f.Close()

	scn := bufio.NewScanner(f)
	for scn.Scan() {
		tmpLn := scn.Text()
		i, _ := strconv.Atoi(tmpLn)
		fData = append(fData, i)
	}

	return fData, nil
}

// file2Read -
func file2Read(e int) (f2r string) {
	switch e {
	case 0:
		return input
	case 1:
		return ex1
	default:
		return ex1
	}
}

// getFlags -
func getFlags() (e int) {
	ex := flag.Int("e", 1, "What example file to use?  1 is default, 0 is prod")
	flag.Parse()
	e = *ex
	return e
}
